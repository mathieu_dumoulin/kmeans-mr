kmeans-mr
=========

 K-Means clustering demo code
 
 Generate data using DataGenerator and then run KMeansClusteringDriver.
 
 This driver iterates over the data to improve the cluster centroids until an
 iteration cutoff or converge
  
 NOTE: the data generator is only making a kind of gaussian cloud around a few
 centroids. This is very easy to solve by kmeans. To solve more difficult
 problems, I'd recommend using a professionnal implementation such as Apache
 Mahout
  
 Based in part on: Thomas Jungblut's blog post 
 see: http://codingwiththomas.blogspot.ca/2011/05/k-means-clustering-with-mapreduce.html
 
 To compile, use maven:
 mvn clean package

 Command to launch data generation:
 hadoop jar target/kmeans-mr-0.2.jar ca.ulaval.ift.graal.kmeans.drivers.DataGenerator -conf conf/kmeans-conf.xml

 Command to launch kmeans
 hadoop jar target/kmeans-mr-0.2.jar ca.ulaval.ift.graal.kmeans.drivers.KmeansClusteringDriver -conf conf/kmeans-conf.xml

 All the parameters are in conf/kmeans-conf.xml

 Tested locally as well as on a Hortonworks Data Platform 1.2 cluster

 Mathieu Dumoulin