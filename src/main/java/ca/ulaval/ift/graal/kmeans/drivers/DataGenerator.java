package ca.ulaval.ift.graal.kmeans.drivers;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.math3.random.RandomGenerator;
import org.apache.commons.math3.random.ValueServer;
import org.apache.commons.math3.random.Well19937c;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.mapred.JobStatus;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.Vector;
import org.apache.mahout.math.VectorWritable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.ulaval.ift.graal.utils.Util;

/**
 * This class is used to create the date to run the algorithm over. The
 * centroids are all hard coded. Next step would be to use the canopy method to
 * determine the number of clusters automatically.
 * 
 * @author Mathieu Dumoulin
 * 
 */
public class DataGenerator extends Configured implements Tool {
	private static final Logger LOG = LoggerFactory
			.getLogger(DataGenerator.class);

	// This value is used to limit the scale of values that populate the vectors
	private static final int MAX_CENTROID_VALUE = 10;

	// This value should match the length of the vectors defined in centroids!
	private static final int VECTOR_LENGTH = 10;
	private static final int N_CENTROIDS = 6;

	// a smaller value of sigma will make the point clouds smaller, the
	// centroids will be easier to find
	private static final double SIGMA = 3;

	// for each centroid, how many points to generate (should be very large if
	// run on a cluster)
	private static final int N_POINTS = 100;

	private static final String DATA_PATH = "data/kmeans/data.seq";
	private static final String INITIAL_CLUSTERS_PATH = "data/kmeans/centroid.seq";

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new DataGenerator(), args);
		System.exit(exitCode);
	}

	@Override
	public int run(String[] arg0) throws IOException, ClassNotFoundException,
			InterruptedException {
		LOG.info("JOB START");
		Configuration conf = getConf();
		FileSystem fs = FileSystem.get(conf);

		Path centroidOutPath = new Path(conf.get("centroid.path",
				INITIAL_CLUSTERS_PATH));
		Path dataOutPath = new Path(conf.get("data.path", DATA_PATH));
		if (fs.exists(dataOutPath))
			fs.delete(centroidOutPath, true);
		if (fs.exists(centroidOutPath))
			fs.delete(dataOutPath, true);

		int centroidCount = conf.getInt("centroid.count", N_CENTROIDS);
		int vectorLength = conf.getInt("vector.length", VECTOR_LENGTH);
		int dataVectorsCount = conf.getInt("data.vectors.count", N_POINTS);

		SequenceFile.Writer centroidOut = null;
		SequenceFile.Writer dataOut = null;

		try {
			centroidOut = SequenceFile.createWriter(fs, conf, centroidOutPath,
					LongWritable.class, VectorWritable.class);
			dataOut = SequenceFile.createWriter(fs, conf, dataOutPath,
					LongWritable.class, VectorWritable.class);

			ArrayList<Vector> centroids = generateCentroids(centroidOut,
					centroidCount, vectorLength);
			generateDataVectors(dataOut, centroids, vectorLength,
					dataVectorsCount);

		} finally {
			if (centroidOut != null) {
				centroidOut.close();
			}
			if (dataOut != null) {
				dataOut.close();
			}
		}

		LOG.info("SUCCEEDED!");

		System.out.println("Showing the centroids:");
		Util.showClusters(conf, centroidOutPath);

		return JobStatus.SUCCEEDED;
	}

	private void generateDataVectors(SequenceFile.Writer dataOut,
			ArrayList<Vector> centroids, int vectorLength, int dataVectorsCount)
			throws IOException {
		RandomGenerator randomData = new Well19937c();
		ValueServer valueServer = new ValueServer(randomData);
		valueServer.setSigma(SIGMA);

		for (int i = 0; i < centroids.size(); i++) {
			for (int j = 0; j < dataVectorsCount; j++) {
				double[] values = new double[vectorLength];
				for (int k = 0; k < vectorLength; k++) {
					values[k] = randomData.nextGaussian()
							+ centroids.get(i).get(k);
				}

				Vector v = new DenseVector(values);
				dataOut.append(new LongWritable((i + 1) * j),
						new VectorWritable(v));
				LOG.debug("New vector added: " + v.toString());
			}
		}
	}

	ArrayList<Vector> generateCentroids(SequenceFile.Writer centroidOut,
			int nCentroids, int vectorLength) throws IOException {
		RandomGenerator random = new Well19937c();
		ArrayList<Vector> centroids = new ArrayList<Vector>();
		for (int centroidID = 0; centroidID < nCentroids; centroidID++) {
			double[] values = new double[vectorLength];
			for (int i = 0; i < vectorLength; i++) {
				values[i] = random.nextInt(MAX_CENTROID_VALUE);
			}
			Vector centroid = new DenseVector(values);
			centroids.add(centroid);
			centroidOut.append(new LongWritable(centroidID),
					new VectorWritable(centroid));
		}
		return centroids;
	}
}
