package ca.ulaval.ift.graal.kmeans.drivers;

import org.apache.hadoop.util.ToolRunner;

public class KmeansMain {

	public static void main(String[] args) throws Exception {
		int exitCode = -1;
		if (args[0].equals("kmeans")) {
			exitCode = ToolRunner.run(new KmeansClusteringDriver(), args);
		} else if (args[0].equals("generator")) {
			exitCode = ToolRunner.run(new DataGenerator(), args);
		} else {
			System.out.println("Usage: [kmeans|generator] conf-file.xml");
		}
		System.exit(exitCode);
	}

}
